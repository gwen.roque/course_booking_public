
let firstName= document.querySelector('#firstName');
let lastName= document.querySelector('#lastName');
let password= document.querySelector('#password1');
let verifyPassword = document.querySelector('#password2');
let passErr = document.querySelector("#passErr");
let email= document.querySelector('#userEmail');

const checkFirstName = function(){
  if (firstName.value.length < 1) {
    firstName.classList.add("error");
    firstName.classList.remove("correct");
 	 } else  firstName.classList.add("correct");
}


const checkLastName = function() {
  if (lastName.value.length < 1) {
    lastName.classList.add("error");
    lastName.classList.remove("correct");
  } else lastName.classList.add("correct");
 }


const checkEmail = function() {
  let emailValue = email.value;
  let pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (!pattern.test(emailValue)) {
    email.classList.add("error");
    email.classList.remove("correct");
  } else  email.classList.add("correct");
 }

const checkMatch = function() {
  if (password.value != verifyPassword.value) {
    verifyPassword.classList.add("error");
    verifyPassword.classList.remove("correct");
  } else verifyPassword.classList.add("correct");
  }


function checkPW() {
    var p = document.getElementById('newPassword').value,
        errors = [];
    if (p.length < 8) {
        errors.push("Your password must be at least 8 characters");
    }
    if (p.search(/[a-z]/i) < 0) {
        errors.push("Your password must contain at least one letter."); 
    }
    if (p.search(/[0-9]/) < 0) {
        errors.push("Your password must contain at least one digit.");
    }
    if (errors.length > 0) {
        alert(errors.join("\n"));
        return false;
    }
    return true;
}
// const checkPW = function() {
// 		let passw = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,15})/;
// 		if(password.value.match(passw)) { 
// 			password.classList.add("error");
//     		if(passErr){
// 				passErr.remove();
// 				verifyPasswxord.classList.add("correct");
// 				return true;
// 		}
// 		} else { 

// 			password.classList.add("error");
// 			passErr.innerHTML = (`
// 				  <p class="invalid">Password must contain the following:</p><br>
// 				  <p class="invalid">A lowercase letter;</p><br>
// 				  <p class="invalid">An uppercase letter;</p><br>
// 				  <p class="invalid">A number and a special character;</p><br>
// 				  <p class="invalid">Minimum 8 & maximum of 15 characters</p><br>
// 				`); 
// 			return false;
// 	}
// }

verifyPassword.addEventListener("keyup", checkMatch);
password.addEventListener("change", checkPW);
email.addEventListener("keyup", checkEmail);
firstName.addEventListener("keyup", checkFirstName);
lastName.addEventListener("keyup", checkLastName);


let registerForm = document.querySelector("#registerUser");
let token = localStorage.getItem("token")

registerForm.addEventListener("submit", (e)=> {
	e.preventDefault()//to avoid page refresh/redurection once that the said event has been triggered.

	let firstName= document.querySelector('#firstName').value
	let lastName= document.querySelector('#lastName').value
	let email= document.querySelector('#userEmail').value
	let mobileNo = document.querySelector('#mobileNumber').value
	let password= document.querySelector('#password1').value
	let verifyPassword = document.querySelector('#password2').value
	


	if((firstName !== "" && lastName !== "" && email !== "" && password !== "" && verifyPassword !== "") && (password === verifyPassword) && (mobileNo.length === 11)){
		
				fetch('https://powerful-retreat-79036.herokuapp.com/api/users/email-exists', {
					method: 'POST',
					headers:{
						'Content-Type': 'application/json'
						},
					body:JSON.stringify({
						email: email
						})
					}).then(res => {
						return res.json() //to make it readable once the response returns to the client side.
					}).then(convertedData => {
						//what would the response look like
						// create a control structure to determine the proper procedure depending on the response
						if (convertedData === false) {
							//let user to register an account

							//create a new account for the user using the data that the user entered

							//url -> describes the desitation of the request.
							fetch('https://powerful-retreat-79036.herokuapp.com/api/users/register', { 
								//the structure of our request for register
								method: 'POST', 
								headers: {
									'Content-Type':'application/json'
									},

									//API only accepts request in a string format.
									body: JSON.stringify({
										//key-value pair
										firstName: firstName,
										lastName: lastName,
										email: email,
										mobileNo: mobileNo,
										password: password
									})

								}).then(res => {
									console.log(res)
						
									return res.json()
								}).then(data => {
									console.log(data)
									if(data === true){
										Swal.fire("New Account Registered successfully!")
										
										window.location.replace("./login.html")
									} else {
										Swal.fire("something went wrong during processing!")
									}
								})

							} else {
								Swal.fire({
									icon: "error",
									title: "Ooopppss!",
									text: "Email already exists!!!"
									})
								}
						})

				} else {
					Swal.fire({
						icon: "error",
						title: "Ooopppss!",
						text: "incomplete information!!"
					})
				}
			
			})


