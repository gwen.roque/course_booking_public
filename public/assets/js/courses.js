//what will be the first task in retrieving all records inside the course collection

//target the container from the html document
let container = document.querySelector('#coursesContainer')

let adminControls = document.querySelector('#adminButton')

//lets determine if there is a user currently logged in.
//let capture one of the properties that is currently stored in our web storage

const isAdmin = localStorage.getItem("isAdmin")

//control structure to determine the display in the front end
let token = localStorage.getItem("token")
let navItem = document.querySelector('#navSession')


if (!token) {
	navItem.innerHTML = `
	<li class="nav-item"><a href="./login.html" class="nav-link">Login</a></li>
	`
window.location.replace("./login.html")
} else {



			if (isAdmin == "false" || !isAdmin) {
				adminControls.innerHTML = null;

			} else {
				adminControls.innerHTML =
				`
				<div class="col-md-2 offset offset-md-10"><a href="./../pages/addCourse.html" class="btn btn-block btn-danger">Add Course</a></div>
				`
			}


			//send a request to retrieve all documents from the courses collection

			fetch('https://powerful-retreat-79036.herokuapp.com/api/courses/allCourses').then(res => res.json()).then(jsonData => {
				console.log(jsonData)


				let courseData;
				//control structure that will determine the value that the var will hold

				if (jsonData.length < 1) {
					console.log("no courses available")
					courseData = "No courses Availabe"
					container.innerHTML = courseData
				} else {
					//if the condition given is not met, dispaly the contents of the arrray inside our page
					//iterate courses collection to display each course collection and display each course inside the browser


					courseData = jsonData.map(course =>{
						//lets check the makeup/structire of eacah element inside the courses collection
						console.log(course._id);
					let cardFooter;

					if (isAdmin == "false" || !isAdmin) {
					cardFooter =
							`<a href="./course.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">View Course Details</a>`
						
					}else{
					cardFooter =
						`
						<a href="" class="btn btn-warning text-white btn-block">Edit Course</a>
						<a href="" class="btn btn-danger text-white btn-block">Delete Course</a>
						`
					}
					return(
						`
						<div class="col-md-5 my-3">
							<div class="card">
								<div class="card-body">
									<h3 class="card-title">Course Name: ${course.name}</h3>
									<p class="card-text text-left">Price: ${course.price} </p>
									<p class="card-text text-left">Description:${course.description} </p>
									<p class="card-text text-left">Created on: ${course.createdOn}</p>
								</div>
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>`)
					

					
					}).join("") //to create a return of a new string. concatenated all objs inside the array and converted each to a string data type

						container.innerHTML = courseData;
				}
			
			})
		navItem.innerHTML =`
		<ul class="navbar-nav ml-auto">
			<li class="nav-item"><a href="./profile.html" class="nav-link">Profile Page</a></li>
			<li class="nav-item"><a href="./logout.html" class="nav-link">Logout</a></li>
		</ul>
	`
}
