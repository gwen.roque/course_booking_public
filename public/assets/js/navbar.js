// console.log("hello from navbar js")

//captured navsession from navbar comp
let navItem = document.querySelector('#navSession')

//to check if a user is currenlty logged in
let userToken = localStorage.getItem("token")

if (!userToken) {
	navItem.innerHTML = `
	<li class="nav-item"><a href="./login.html" class="nav-link">Login</a></li>
	`
} else {
	navItem.innerHTML =`
	<li class="nav-item"><a href="./logout.html" class="nav-link">Logout</a></li>
	`
}