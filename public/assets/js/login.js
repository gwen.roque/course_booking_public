console.log("Hello from JS");


//Lets target our form component inside our document
let loginForm = document.querySelector('#loginUser');

//login form will be used by the client to insert his/acct to be authenticated by your app.

loginForm.addEventListener("submit", (event) =>{
	event.preventDefault()

	//lets capture the values of our form components.

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value	
	let message7 = document.querySelector(".message7");

	//checker to confirm he acquired values
	// console.log(email)
	// console.log(password)


	//validate the data inside the input fields

	if (email !== "" || password !== "") {
		//send a request to the desired end point

		fetch("https://powerful-retreat-79036.herokuapp.com/api/users/login", {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify ({
				email: email,
				password: password
			})
		}).then(res => {
			return res.json()
		}).then(dataConverted => {
			// console.log(convertedData.accessToken)
			//check if the conversion of the response has been successful
			// console.log(convertedData)
			//save the generated access token inside the local storage property of the browser.
			if (dataConverted.accessToken) {
				localStorage.setItem('token', dataConverted.accessToken)

				//confirmation of the previous task	
				/*Swal.fire({
					icon: "success",
					title: "Yep",
					text: "Suuceessssfully generated access token!!!!"
					})*/

				//make sure to identify rthe access rights of the user before you redirect the user to another page/location
				//how to confirm if a user is an admin or not

				fetch('https://powerful-retreat-79036.herokuapp.com/api/users/details', {
					headers: {
						'Authorization' : `Bearer ${dataConverted.accessToken}`
					} //upon sending the req, we are instantiating a promise taht can lead to 2 possible outcomes; What needs to be done to handle the possible outcome states of this promise
				}).then(res => {
					return res.json()
				}).then(data => {
					//to check if we are able to get the user's data
					console.log(data)
					//fetching the user's info is a success
					//save the "id", "isAdmin"
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)

					//checker
					// console.log("items are succesfully stored ")

					//how would you redirect the user to another location
				window.location.replace('./../pages/profile.html')
				
				})


				

			} else {
				//this block of code will run if no access token was generated
				popUp();
				function popUp(){
					Swal.fire({
					icon: "error",
					title: "Invalid Password",
					text: " Please Try Again!!!!"
					})
					clearForm();
				}

				function clearForm(){
					
					let emailField = document.querySelector("#userEmail");
					let passwordField = document.querySelector("#password");
					
					emailField.value = ' ';
					passwordField.value = '';

				}
				

			}
		})


	} else {
		Swal.fire({
					icon: "error",
					title: "Ooopppss!",
					text: "Please input your email and/or password first!!!!"
			})
		

	}

})

