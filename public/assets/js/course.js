let urlValues =  new URLSearchParams(window.location.search)
//lets check what the structure of this variable will look like
//console.log(urlValues) //checker

//lets get only the desired data from the object using a get()
let id = urlValues.get('courseId')
console.log(id) //checker

let token = localStorage.getItem('token')
let userId =localStorage.getItem('id');
console.log(userId)

//containers for all the details that we would want to display. 
let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let desc = document.querySelector("#courseDesc")
let enroll = document.querySelector("#enrollmentContainer")


function findCourseId(array, key, value){
		for(var i =0; i  < array.length; i++){
			if(array[i][key] === value){
				return array[i];
			}
		}
		return null
	}


fetch(`https://powerful-retreat-79036.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(convertedData => {
	console.log(convertedData)
	
			name.innerHTML = `<h5> Course Name: </h5>  ${convertedData.name}`
			price.innerHTML = `<h5> Price: </h5>  ${convertedData.price}`
			desc.innerHTML = `<h5> Description: </h5> ${convertedData.description}`	

    fetch('https://powerful-retreat-79036.herokuapp.com/api/users/details', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			}
			}).then(res=>res.json()).then(userData=> {
				userEnrolled = userData.enrollments
		
				let enrollmentDetails = findCourseId(userEnrolled, 'courseId', id);
				// console.log('test 2 ');
				// let test = userEnrolled.filter(item => item.courseId === id);
				// console.log(test);

				if(enrollmentDetails != null){
					enroll.innerHTML = ` <div id="course">
					<p><h5> Status:</h5> ${enrollmentDetails.status}</p>
					<p><h5>Date Enrolled:</h5> ${enrollmentDetails.enrolledOn}`	

				}
				else{
					enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`
					enrollBtn();
				}

		});

	
})

	function enrollBtn(){

	document.querySelector("#enrollButton").addEventListener("click", () => {

		//insert the course inside the enrollments array of the user
			fetch('https://powerful-retreat-79036.herokuapp.com/api/users/enroll', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`//we have to get the actual value of the token variable
				},
				body: JSON.stringify({
					courseId: id 
				})
			})
			.then(res => {
				return res.json()
			}).then(convertedResponse => {
				console.log(convertedResponse)
				//lets create a control structure that will determine a response according to the result that will diplayed to the client.
			
					if (convertedResponse === true) {
		               alert("Thank you for enrolling")

		               //we can redirect the user back to the courses page
		               window.location.replace("./courses.html")
					} else {
		              //inform the user if the enrollment failed. 
		              alert("something went wrong!")
				}
			
			})
		
		})
	}


