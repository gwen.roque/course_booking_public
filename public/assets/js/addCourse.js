

const isAdmin = localStorage.getItem("isAdmin")
 


if (isAdmin == "false" || !isAdmin) {
	window.location.replace('./../pages/login.html')

	} else {

	//create our addcourse page
	let formSubmit = document.querySelector('#createCourse');

	//lets acquire an event that will be applied in our form component. 
	//create a subfunction inside the method to describe the procedure/action that will take place upon triggering the event.
	formSubmit.addEventListener("submit", (mangyayari) => {
		mangyayari.preventDefault()//this will avoid page redirection/refresh
		// alert("try lang!")

		//lets target the values of each component inside the forms.
		let name = document.querySelector("#courseName").value
		let cost = document.querySelector("#coursePrice").value
		let desc = document.querySelector("#courseDesc").value

	//create a logic that will make sure that the needed information is sufficient from our form component. and make sure to inform the user what went wrong.



		
	if(name !== "" && cost !== "" && desc !==""){

		fetch('https://powerful-retreat-79036.herokuapp.com/api/courses/course-exist', {
				method: 'POST',
				headers:{
					'Content-Type': 'application/json'
				},
				body:JSON.stringify({
					name: name
				})
			}).then(res => {
				return res.json() //to make it readable once the response returns to the client side.
			}).then(convertedCourseData => {

				// lets create a checker to see if we were able to capture the values of each input fields.
				// console.log(name)	
				// console.log(cost)
				// console.log(desc)

				//send a request to the backend project to process the data for saving

				if (convertedCourseData === false) {
				//upon creating this fetch api request, we are instantiating a promise
					fetch('https://powerful-retreat-79036.herokuapp.com/api/courses/create', {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							name: name,
						 	description: desc,
					 		price: cost
						})
					}).then(res => { 
						//to answer the question below, lets observe first the structure of the response
						// console.log(res)
						return res.json() //why do we need to convert the response into a json format?
					}).then(info => {
						console.log(info)
						//lets check what the response looks like pag naaplyan na sya ng json()
						//lets crete a control structure that will desc the repsponse of the UI to the client
						if (info === true) {
							Swal.fire({
							icon: "success",
							title: "Yaaaay!",
							text: "Course Successfully Registered!"
							})
						} else {
						Swal.fire({
							icon: "error",
							title: "Course Registration Failed!",
							text: "Try Again! "
							})
						}

					})
				}else {
						Swal.fire({
							icon: "error",
							title: "Course Registration Failed",
							text: "Course name Exists"
							})
						}
				})

			}else{
				Swal.fire({
							icon: "error",
							title: "Course Registration Failed",
							text: "Please Try Again"
							})
						
			}	



	})
}
	//how can we handle the promise object that will be returned once the fetch method event has happened?