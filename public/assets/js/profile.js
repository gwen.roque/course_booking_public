//get the value of the access token inside the local storage and place it inside a new variable

let token = localStorage.getItem("token")
let navItem = document.querySelector('#navSession')
let container = document.querySelector("#profileContainer")

const isAdmin = localStorage.getItem("isAdmin")
		let output;

		if (isAdmin == "false" || !isAdmin ) {

			
			fetch ('https://powerful-retreat-79036.herokuapp.com/api/users/details', {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					}
				}).then(res => res.json())
				.then(jsonData=> {
				
					console.log(jsonData)
					console.log(jsonData.enrollments)
					if (jsonData) {

					
					let userSubjectPromises = jsonData.enrollments.map(subject => {
				
						return fetch (`https://powerful-retreat-79036.herokuapp.com/api/courses/${subject.courseId}`).then(res => res.json())
							.then(subjectData=> {
								
							
								return (`
								<tr>
									<td>${subjectData.name}</td></td>
									<td>${subjectData.description}</td>
									<td>${subject.enrolledOn}</td>
									<td>${subject.status}</td>
								</tr>
								`)
							})
						}); 
						
						return Promise.all(userSubjectPromises).then(results => {
							const newHtml = results.join("");
							document.getElementById("profileContainer").innerHTML = `
							<div class="col-md-13">
								<span><h3>User Profile:</h3></span>
								<section id="profileData" class="jumbotron my-5">
									<h4 class="text-center">First Name: ${jsonData.firstName}</h4>
									<h4 class="text-center">Last Name:${jsonData.lastName} </h4>
									<h4 class="text-center">Email: ${jsonData.email} </h4>
									<h4 class="text-center">MobileNum:${jsonData.mobileNo} </h4>
									<table class="table">
										<tr>
										
											<th>Course Name: </th>
											<th>Course Description: </th>
											<th>Enrolled On:</th>
											<th>Status:</th>
											<tbody> ${newHtml} </tbody>
										</tr>
									</table>

								</section>
							</div>
							`;
						});

			navItem.innerHTML =`
			<ul class="navbar-nav ml-auto">			
			<li class="nav-item"><a href="./courses.html" class="nav-link">Courses</a></li>
			<li class="nav-item"><a href="./logout.html" class="nav-link">Logout</a></li>
			</ul
			`
		} else {
		// localStorage.clear()
		window.location.replace("./courses.html")
	}
}).catch(err => {
	console.log(err);
});

}